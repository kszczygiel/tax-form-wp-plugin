<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://gitlab.com/kszczygiel
 * @since      1.0.0
 *
 * @package    Tax_form
 * @subpackage Tax_form/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tax_form
 * @subpackage Tax_form/includes
 * @author     Krzysztof Szczygieł <krzysiek.szczygiel96@gmail.com>
 */
class Tax_form_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

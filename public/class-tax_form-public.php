<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://gitlab.com/kszczygiel
 * @since      1.0.0
 *
 * @package    Tax_form
 * @subpackage Tax_form/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Tax_form
 * @subpackage Tax_form/public
 * @author     Krzysztof Szczygieł <krzysiek.szczygiel96@gmail.com>
 */
class Tax_form_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tax_form_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tax_form_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tax_form-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tax_form_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tax_form_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tax_form-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js' , $this->version, false );

	}

	public function register_tax_form_shortcode(){
		add_shortcode('tax_form', array( $this, 'generate_tax_form_view')); 
	}


	public function generate_tax_form_view($atts){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		include_once( 'partials/tax_form-public-display.php' );
	}

	public function register_tax_form_cpt(){
		{

							$labels = array(
									'name' => _x('Tax forms', 'Post Type General Name'),
									'singular_name' => _x('Tax form', 'Post Type Singular Name'),
									'menu_name' => __('Tax forms'),
									'parent_item_colon' => __('Parent form'),
									'all_items' => __('All tax forms'),
									'view_item' => __('View tax form'),
									'add_new_item' => __('add new'),
									'add_new' => __('Add new'),
									'edit_item' => __('Edit'),
									'update_item' => __('Update'),
									'search_items' => __('Search'),
									'not_found' => __('Nothing found'),
									'not_found_in_trash' => __('Not found in trash'),
							);
			
			// Set other options for Custom Post Type
			
							$args = array(
									'label' => __('tax_form'),
									'description' => __('Tax forms'),
									'labels' => $labels,
									// Features this CPT supports in Post Editor
									'supports' => array('title', 'editor', 'custom-fields' ),
									// You can associate this CPT with a taxonomy or custom taxonomy.
									/* A hierarchical CPT is like Pages and can have
									* Parent and child items. A non-hierarchical CPT
									* is like Posts.
									*/
									'hierarchical' => false,
									'public' => true,
									'show_ui' => true,
									'show_in_menu' => true,
									'show_in_nav_menus' => true,
									'show_in_admin_bar' => true,
									'menu_position' => 5,
									'can_export' => true,
									'has_archive' => false,
									'show_in_rest' => true,
									'exclude_from_search' => false,
									'publicly_queryable' => true,
									'capability_type' => 'post',
									'menu_icon' => 'dashicons-editor-ul'
							);
			
							// Registering your Custom Post Type
							register_post_type('tax_form', $args);
					}
	}

	public function ajax_save_tax_form(){
		$params = [];
		parse_str($_POST['data'], $params);

		$text = '';

		foreach($params as $paramKey => $param){
			$text.= $paramKey . ' : ' . $param . ' <br> ';
		}
		
		$my_cptpost_args = array(
			'post_title' => 'Kalkulacja ' . date('m/d/Y h:i:s a', time()) . ' dla ' . $params['taxFormIP'],
			'post_status' => 'publish',
			'post_type' => 'tax_form',
			'post_content' => $text,
			'post_author' =>'0',
			'meta_input' => $params
		);

		$cpt_id = wp_insert_post($my_cptpost_args);

		if ($cpt_id) {
			echo 'saved';
			exit();
		} else {
			echo 'nope :(';
			exit();
		}
	}
}


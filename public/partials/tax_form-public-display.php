<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://gitlab.com/kszczygiel
 * @since      1.0.0
 *
 * @package    Tax_form
 * @subpackage Tax_form/public/partials
 */
?>
<script type="text/javascript">
           var ajaxUrl = '<?php echo admin_url('admin-ajax.php') ?>';
</script>
<form id="taxForm" class="tax-form">
    <div id="taxFormOutput" class="tax-form-output">
    
    </div>
    <div class="input-wrapper">
      <label for="taxFormName">
        <?php _e('Product Name', 'tax_form')?>
      </label>
      <input type="text" id="taxFormName" name="taxFormName">
    </div>
    <div class="input-wrapper">
      <label for="taxFormPrice">
        <?php _e('Net price', 'tax_form')?>
      </label>
      <input type="number" id="taxFormPrice" name="taxFormPrice">
    </div>
    <div class="input-wrapper">
      <label for="taxFormCurrency">
        <?php _e('Currency', 'tax_form')?>
      </label>
      <input type="text" id="taxFormCurrency" name="taxFormCurrency" disabled value="PLN">
    </div>
    <div class="input-wrapper">
      <label for="taxFormRate" id="taxFormRate">
        <?php _e('VAT rate', 'tax_form')?>
      </label>
      <select name="taxFormRate" id="taxFormRate">
          <option value="0.0">0%</option>
          <option value="0.03">3%</option>
          <option value="0.05">5%</option>
          <option value="0.07">7%</option>
          <option value="0.08">8%</option>
          <option value="0.22">22%</option>
          <option value="0.23">23%</option>
      </select>
    </div>

    <input type="hidden" name="taxFormIP" value="<?php echo $ip ?>">

    <button class="btn-tax-form" type="submit"><?php _e('Calculate', 'tax_form')?></button>
</form>
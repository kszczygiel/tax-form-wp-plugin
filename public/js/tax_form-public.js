(function( $ ) {
	'use strict';

	$(document).ready(function(){
		$('#taxForm').submit(function(e) {
			e.preventDefault();
			var formData = [];

			$(this).find('input, select').each(function(){
				formData[$(this).attr('name')] = $(this).val();
			});

			var vatValue = Number(formData['taxFormPrice']) * Number(formData['taxFormRate']),
					grossPrice = Number(formData['taxFormPrice']) + vatValue;

			var formOutput = `Cena produktu ${formData['taxFormName']},
			wynosi: ${grossPrice} zł brutto, kwota podatku to ${vatValue} zł.`;

			$('#taxFormOutput').html(formOutput).addClass('success');

			jQuery.ajax({
				url: ajaxUrl,
				data: {
					action: 'ajax_save_tax_form',
					data: $('#taxForm').serialize()
				},
				type: 'POST',
				success:function (data) {
					console.log(data);
				}
			});
		});
	});



})( jQuery );

<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://gitlab.com/kszczygiel
 * @since      1.0.0
 *
 * @package    Tax_form
 * @subpackage Tax_form/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

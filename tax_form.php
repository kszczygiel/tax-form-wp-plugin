<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://gitlab.com/kszczygiel
 * @since             1.0.0
 * @package           Tax_form
 *
 * @wordpress-plugin
 * Plugin Name:       Tax Form
 * Plugin URI:        https://gitlab.com/kszczygiel/tax-form-plugin
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Krzysztof Szczygieł
 * Author URI:        https://gitlab.com/kszczygiel
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tax_form
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'TAX_FORM_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tax_form-activator.php
 */
function activate_tax_form() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tax_form-activator.php';
	Tax_form_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tax_form-deactivator.php
 */
function deactivate_tax_form() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tax_form-deactivator.php';
	Tax_form_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_tax_form' );
register_deactivation_hook( __FILE__, 'deactivate_tax_form' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tax_form.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_tax_form() {

	$plugin = new Tax_form();
	$plugin->run();

}
run_tax_form();
